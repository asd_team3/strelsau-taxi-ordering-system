class CreateGcmDevice < ActiveRecord::Migration
  def change
    create_table :gcm_devices do |t|
      t.references :taxi
      t.string :device_token
    end
  end
end
