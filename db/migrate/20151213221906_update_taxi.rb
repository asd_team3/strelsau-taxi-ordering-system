class UpdateTaxi < ActiveRecord::Migration
  def change
    change_table :taxis do |t|
      t.references :user
    end
  end
end
