class CreateBookingDecision < ActiveRecord::Migration
  def change
    create_table :booking_decisions do |t|
      t.references :taxi
      t.references :booking
      t.integer :status, default: 1
      t.timestamps null: false
    end
  end
end
