class CreateBooking < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.references :user
      t.references :taxi
      t.string :phone
      t.decimal :longitude
      t.decimal :latitude
      t.column :status, :integer, default: 0

    end
  end
end
