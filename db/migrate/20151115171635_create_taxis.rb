class CreateTaxis < ActiveRecord::Migration
  def change
    create_table :taxis do |t|
      t.decimal :latitude
      t.decimal :longitude
      t.integer :status, default: 1
    end
  end
end
