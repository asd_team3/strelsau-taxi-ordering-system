class TaxiCoords < BaseDto
  attr_accessor :latitude, :longitude, :device_token
end