class Route < BaseDto
  attr_accessor :distance_text, :duration_text
end