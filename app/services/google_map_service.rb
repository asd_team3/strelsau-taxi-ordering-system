class GoogleMapService

  URL = "https://maps.googleapis.com/maps/api/distancematrix/json?"

  def get_route (booking, taxi)
    client_lat = booking.latitude
    client_lng = booking.longitude
    taxi_lat = taxi.latitude
    taxi_lng = taxi.longitude
    url = GoogleMapService::URL+"origins=#{taxi_lat},#{taxi_lng}&destinations=#{client_lat},#{client_lng}&mode=driving"
    url_parsed = URI.parse(url)
    result=Net::HTTP.get_response(url_parsed).body
    result = JSON.parse(result)
    row = result["rows"][0]
    if find_value(row,"status") != "OK"
      puts "google map error occured"
      return nil
    end
    Route.new(distance_text: find_value(row,"distance")["text"], duration_text: find_value(row,"duration")["text"])
  end

  def get_distances(taxis, booking)
    taxi_distances=[]
    params="origins="
    taxis.each do |t|
      params = %Q[#{params}#{t.latitude},#{t.longitude}|]
    end
    params = %Q[#{params}&destinations=#{booking.latitude},#{booking.longitude}&mode=driving]
    url_parsed = URI.parse(GoogleMapService::URL+params)
    result=Net::HTTP.get_response(url_parsed).body
    result = JSON.parse(result)
    result["rows"].each_with_index do |row, index|
      if find_value(row,"status") != "OK"
        next
      end
      distance = find_value(row,"distance")["value"]
      res = DistanceContainer.new
      res.distance = distance
      res.taxi=taxis[index]
      taxi_distances.push(res)
    end
    taxi_distances
  end

  def find_value(row, key)
    elements = row["elements"]
    elements.each do |e|
      if e[key].present?
        return e[key]
      end
    end
    nil
  end

end