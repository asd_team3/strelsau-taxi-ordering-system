class TaxiNotificationProcess

  OFFER_TIME_TO_LIVE = 30
  TAXI_ORDER_MAX_TRIES = 2

  def notify(booking)
    Thread.new{
      tried_taxis=[]
      status = :no_answer
      TaxiNotificationProcess::TAXI_ORDER_MAX_TRIES.times do |try|
        taxi = TaxiService.new.get_suitable_taxi(booking, tried_taxis)

        if taxi.nil?
          break
        end
        puts "NotificationService: taxi selected #{taxi.inspect}"
        tried_taxis.push(taxi)
        decision = BookingDecision.create!(booking: booking, taxi: taxi, status: :no_answer)
        notify_taxi(taxi, booking)
        status=monitor_decision_status(decision.id)
        set_decision_status(decision, status)
        puts "NotificationService: taxi #{taxi.inspect} decision status #{status}"
        if status=="no_answer"
          #TODO: uncomment after
          #make_taxi_off_duty(taxi)
        elsif status=="confirmed"
          booking.status = :confirmed
          booking.taxi = taxi
          booking.save!
          #TODO: update taxi status
          break
        end
      end

      #create the last decision without taxi in order to process driver decision request
      BookingDecision.create!(booking: booking, taxi: nil, status: :no_answer)

      if status != "confirmed" && tried_taxis.count >= TaxiNotificationProcess::TAXI_ORDER_MAX_TRIES
        process_no_confirmations(booking)
      elsif status != "confirmed"
        process_no_taxis(booking)
      end
      notify_client(booking)
    }
  end

  def notify_client(booking)
    PusherService.new.push(booking.id,booking.status)
  end

  def notify_taxi(taxi, booking)
    gcm_device = GcmDevice.where(taxi: taxi).first
    if gcm_device.present? && gcm_device.device_token.present?
      message = {:booking_id => booking.id}
      NotificationService.new.send_data(gcm_device.device_token, message)
    end
  end

  def make_taxi_off_duty(taxi)
    taxi.status = :off_duty
    taxi.save!
  end

  def process_no_taxis(booking)
    booking.status = :rejected_no_taxis
    booking.save!
  end

  def process_no_confirmations(booking)
    booking.status = :rejected_no_confirmations
    booking.save!
  end

  def set_decision_status(decision,status)
    decision.status = status
    decision.save!
  end

  def monitor_decision_status(decision_id)
    status = :no_answer
    TaxiNotificationProcess::OFFER_TIME_TO_LIVE.times do |i|
      sleep(1)
      status=BookingDecision.find(decision_id).status
      if status != "no_answer"
        return status
      end
    end
    status
  end

end