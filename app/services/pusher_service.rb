class PusherService
  def push(id,status)
    puts "PUSHER, sending message with id #{id} and status #{status}"
    Pusher.trigger(id,status,{})
  end
end