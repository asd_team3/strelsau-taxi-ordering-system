class TaxiService

  def pickup(booking_id)
    booking = Booking.find(booking_id)
    puts booking.inspect
    booking.status = :completed
    booking.save!
    PusherService.new.push(booking.id,booking.status)
  end

  def update_coords(taxi_coords)
    taxi = GcmDevice.where(device_token: taxi_coords.device_token).first.taxi
    taxi.latitude = taxi_coords.latitude
    taxi.longitude = taxi_coords.longitude
    taxi.status = :available
    taxi.save!
  end

  def make_decision(decision_dto, taxi)
    booking=Booking.find(decision_dto.booking_id)
    all_booking_decisions = BookingDecision.where(booking: booking)
    last_decision = all_booking_decisions.order(:created_at).reverse.first

    if decision_dto.confirmed == 'true'
      if last_decision.present? && last_decision.taxi==taxi
        last_decision.status = :confirmed
        last_decision.save!
        booking.taxi = taxi
        booking.status = :confirmed
        return true
      end
    else
      last_decision.status = :rejected
      last_decision.save!
      return true
    end
    false
  end

  def get_suitable_taxi(booking, excluded_taxis)
    sorted = get_sorted_nearest_taxis(booking, excluded_taxis)
    if sorted.nil? || sorted.empty?
      return nil
    end
    distance = nil
    first_same_distances = []

    sorted.each do |t|
      if distance.nil?
        distance = t.distance
      else
        if t.distance != distance
          break
        end
      end
      first_same_distances.push(t)
    end

    if first_same_distances.count == 1
      return first_same_distances.first.taxi
    end
    first_same_distances.sort_by{ |t| get_standby_time(t)}.reverse.first.taxi
  end

  def get_sorted_nearest_taxis(booking, excluded_taxis)
    all_distances = get_taxi_distances(get_active_taxis(excluded_taxis), booking)
    all_distances.sort_by{ |l| l.distance }
  end

  def get_taxi_distances(taxis, booking)
    GoogleMapService.new.get_distances(taxis, booking)
  end

  def get_active_taxis(excluded_taxis)
    all_active=Taxi.available.where.not(longitude: nil).where.not(latitude: nil).all
    all_active.select{|t| excluded_taxis.exclude? t }
  end

  def get_standby_time(distance_container)
    #updated_at indicates a last taxi interaction with a booking
    last_bookings=Booking.where(taxi: distance_container.taxi).all
    if last_bookings.empty?
      #since there was no requirement about the first booking in scope of measuring a standby time
      return 0
    end
    last_booking = last_bookings.sort_by(:updated_at, :desc).first
    #time in seconds
    ((Time.now - last_booking.updated_at) * 24 * 60 * 60).to_i
  end

end