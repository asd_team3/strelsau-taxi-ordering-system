class BookingService

  def initialize(user)
    @user = User.first
  end

  def book(booking_req)
    booking = Booking.create(user: @user)
    booking.latitude = booking_req.latitude
    booking.longitude = booking_req.longitude
    booking.phone = booking_req.phone
    booking.save!
    TaxiNotificationProcess.new.notify(booking)
    booking
  end



end

