class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: [ :client, :driver]

  def generate_auth_token
    payload = { user_id: self.id }
    AuthToken.encode(payload)
  end

end
