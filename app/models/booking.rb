class Booking < ActiveRecord::Base
  belongs_to :user
  belongs_to :taxi
  enum status: [ :in_process, :confirmed, :completed, :rejected_no_taxis, :rejected_no_confirmations ]
end