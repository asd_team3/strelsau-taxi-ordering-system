class BookingDecision < ActiveRecord::Base
  belongs_to :taxi
  belongs_to :booking
  enum status: [ :no_answer, :rejected, :confirmed]
end