class Taxi < ActiveRecord::Base
    enum status: [ :off_duty, :available, :busy, :invisible ]
    belongs_to :user
end