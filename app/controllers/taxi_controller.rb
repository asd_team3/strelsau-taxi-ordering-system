class TaxiController < ApplicationController
  def update_coords
    taxi_coords = TaxiCoords.new(params[:taxi_coords])
    TaxiService.new.update_coords(taxi_coords)
    render :text => "", :status => :ok
  end

  def make_decision
    decision=TaxiBookingDecision.new(params[:decision])
    taxi = Taxi.where(user: @current_user).first
    confirm_decision = TaxiService.new.make_decision(decision, taxi)
    if confirm_decision
      render :text => "", :status => :ok
    else
      render :text => "", :status => :bad_request
    end
  end

  def make_available
    taxi = Taxi.where(user: @current_user).first
    taxi.status = :available
    taxi.save!
    render :text => "", :status => :ok
  end

  def make_off_duty
    taxi = Taxi.where(user: @current_user).first
    taxi.status = :off_duty
    taxi.save!
    render :text => "", :status => :ok
  end

  def pickup
    TaxiService.new.pickup(params[:booking_id])
    render :text => "", :status => :ok
  end

end