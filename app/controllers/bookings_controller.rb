class BookingsController < ApplicationController

  def create
    booking_req = BookingRequest.new(params[:booking])
    booking = BookingService.new(@current_user).book(booking_req)
    render :text => booking.to_json, :status => :created
  end

  def get_taxi_location
    booking = Booking.find(params[:booking_id])

    if (booking.confirmed? || booking.completed?) && booking.taxi.present?
      route = GoogleMapService.new.get_route(booking, booking.taxi)
    end
    response = BookingTaxiLocation.new(taxi: booking.taxi, route: route)
    render :text => response.to_json, :status => :ok
  end

end