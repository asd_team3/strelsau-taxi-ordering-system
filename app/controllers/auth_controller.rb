class AuthController < ApplicationController
  skip_before_action :authenticate_request # this will be implemented later
  def authenticate
    user = User.find_by_email(params[:username])
    if user.nil?
      render json: { error: 'Invalid username' }, status: :unauthorized
    else
      if user.valid_password?(params[:password])
        render json: { auth_token: user.generate_auth_token }
        flash[:notice] = "Hello #{user[:username]}"
      else
        render json: { error: 'Invalid password' }, status: :unauthorized
      end
    end
  end
end