class RegisterTokenController < ApplicationController

  def create
    device_token = params[:device_token]
    taxi = Taxi.where(user: @current_user).first
    if taxi.present?
      device = GcmDevice.where(taxi: taxi).first
      if device.nil?
        GcmDevice.create!(taxi: taxi, device_token: device_token)
      else
        device.device_token = device_token
        device.save!
      end
      render :text => 'Registered successfully', :status => :ok
    else
      render json: { error: 'No such driver' }, :status => :unauthorized
    end
  end
end