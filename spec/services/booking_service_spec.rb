RSpec.describe BookingService do


  it 'book_should_return_booking' do

    expect(thebook).to eq theuser
  end
  it 'get_nearest_taxis_should_return_sorted_by_distance' do
    taxis = []
    (0..19).each do |i|
      taxis.insert(i, Taxi.create(latitude: 30-i, longitude: 30-i))
      #taxis << Taxi.create(latitude: 30-i, longitude: 30-i)
    end
    service = BookingService.new(double(:user))
    result = service.get_nearest_taxis(taxis)
    expect(result.count).to eq(10)
  end

  it 'sort_by_standby_should_return_taxi_lengths' do
    taxis = []
    (0..19).each do |i|
      taxis.insert(i, Taxi.create(latitude: 30-i, longitude: 30-i))
      #taxis << Taxi.create(latitude: 30-i, longitude: 30-i)
    end
    service = BookingService.new(double(:user))
    result = service.sort_by_standby(taxis)
    expect(result.count).to eql? taxis.length
  end


end