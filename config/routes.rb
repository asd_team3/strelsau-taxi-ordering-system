Rails.application.routes.draw do
  get 'notifications/create' => 'notifications#create'

  devise_for :users
  match '*all' => 'application#cors_preflight', via: :options

  post "bookings" => "bookings#create"
  get "bookings/taxi_location/:booking_id", :to => "bookings#get_taxi_location"

  post 'auth' => 'auth#authenticate'
  post "register_token" => "register_token#create"

  put 'taxis/update' => 'taxi#update_coords'
  put 'taxis/decision' => 'taxi#make_decision'
  put 'taxis/pickup/:booking_id' => 'taxi#pickup'
  put 'taxis/make_available' => 'taxi#make_available'
  put 'taxis/make_off_duty' => 'taxi#make_off_duty'

end
