Feature: Taxi selection with driver confirmation
Background:
    Given I am at Liivi 2
    And there are two taxi available, one next to Kaubamaja and the other at Lounakeskus
    When I submit a booking request
Scenario: The driver of the closest taxi accepts the booking
    When STRS sends the booking request to the closest taxi
    And The taxi driver accepts the booking
    Then I should receive a confirmation with the information of the closest taxi
    And I should receive a delay estimate
Scenario: Taxi driver rejects booking
    When The taxi driver of the closest taxi rejects the booking
    Then STRS should be resent to the other taxi
Scenario: Request timeout
    When STRS sends the booking request to the closest taxi
    And The taxi driver does not reply before 30 seconds
    Then STRS should select another taxi