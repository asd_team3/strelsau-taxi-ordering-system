Feature: Taxi selection
    As a customer
    So that I can have a taxi booking confirmed
    I want one taxi to be selected

Background:
    Given I am in the booking page
    And the following addresses:
  | name            | lat       | lng |
  | Liivi 2         | 2.98      | 6.0  |
  | The Kaubamaja   | 4.0       | 7.0  |
  | Lounakeskus     | 16.3      | 6.9  |
    And I am at Liivi 2

Scenario: Closest taxi
    Given there are two taxi available, one next to Kaubamaja and the other at Lounakeskus
    When I submit a booking request
    Then I should receive a confirmation with the taxi next to Kaubamaja
    And I should receive a delay estimate
Scenario: Multiple candidate taxis
    Given there are two taxi available, at the same distance with respect to Liivi
    When I submit a booking request
    Then I should receive a confirmation with the taxi that has been available the longest
    And I should receive a delay estimate
