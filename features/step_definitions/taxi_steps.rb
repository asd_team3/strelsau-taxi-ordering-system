Given /the following addresses/ do |address_table|
  address_table.hashes.each do |addr|
    Location.create(addr)
  end
end

Given(/^I am at (.*)$/) do |address|
    addr = Location.where(name:address).first
    lat = addr.lat
    lng=addr.lng
    fill_in 'lat-input', with: lat
    fill_in 'lng-input', with: lng
end

Given (/^there are two taxi available, one next to (.*?) and the other at (.*?)$/) do |firstLocation, secondLocation|
  Taxi.create(:location => Location.where(name:firstLocation).first)
  Taxi.create(:location => Location.where(name:secondLocation).first)
end


When(/^I submit a booking request$/) do
  click_button "submit-coord"
end

Then(/^I should receive a confirmation with the taxi next to Kaubamaja$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a delay estimate$/) do
  pending # express the regexp above with the code you wish you had
end

Given(/^there are two taxi available, at the same distance with respect to Liivi$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a confirmation with the taxi that has been available the longest$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^STRS sends the booking request to the closest taxi$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^The taxi driver accepts the booking$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a confirmation with the information of the closest taxi$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^The taxi driver of the closest taxi rejects the booking$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^STRS should be resent to the other taxi$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^The taxi driver does not reply before (\d+) seconds$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

Then(/^STRS should select another taxi$/) do
  pending # express the regexp above with the code you wish you had
end