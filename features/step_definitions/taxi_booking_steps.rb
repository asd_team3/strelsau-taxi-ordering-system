Given(/^I am in the booking page$/) do
  login
end

When(/^I provide my coordinates$/) do
  @client_latitude = 58.3782485
  @client_longitude = 26.7146733
end

When(/^I submit this information$/) do
  post "/bookings", %Q[{"latitude":"#{@client_latitude}":,"longitude":"#{@client_longitude}"}]
end

Then(/^I should be notified that my information is being processed$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should eventually receive an asynchronous message with my address$/) do
  pending # express the regexp above with the code you wish you had
end

def login
  #TODO: genererate users
  post "/auth", %q[{"username":"user1@example.com","password":"user1234"}]
end
