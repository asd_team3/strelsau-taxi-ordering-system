Given(/^I have registered$/) do
  current_user = User.create(:email => 'nice@gmail.com', :password => 'shiny_user')
end

When(/^I submit my username "(.*?)" and password "(.*?)"$/) do |username, password|
  header 'Content-Type', 'application/json'
  post '/auth', %Q[{"username":"#{username}","password":"#{password}"}]
end

Then(/^the response should be "(.*?)"$/) do |status|
  last_response.status.should == status.to_i
end

And(/^the "(.*?)" should be not nil$/) do |token|
  json = ActiveSupport::JSON.decode(last_response.body)
  json[token].should_not == nil
end

And(/^I should see "(.*?)"$/) do |error_message|
  json = ActiveSupport::JSON.decode(last_response.body)
  json['error'].should == error_message
end