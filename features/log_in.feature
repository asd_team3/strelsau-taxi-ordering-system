Feature: Log in
  As a user
  So I want to log in into the taxi system

  Background:
    Given I have registered

  Scenario: Logging in
    When I submit my username "nice@gmail.com" and password "shiny_user"
    Then the response should be "200"
    And the "auth_token" should be not nil

  Scenario: Invalid user
    When I submit my username "not_nice@gmail.com" and password "shiny_user"
    Then the response should be "401"
    And I should see "Invalid username"

  Scenario: Invalid password
    When I submit my username "nice@gmail.com" and password "not_shiny_user"
    Then the response should be "401"
    And I should see "Invalid password"