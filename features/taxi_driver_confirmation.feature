Feature: Driver accepts or rejects a drive request
	As a driver
	So I can drive a customer
	I want to decide on a requested drive

Background:
  	Given I am at TaxiHome homepage
  	And a request for drive is sent

Scenario: Accept Booking
  	Given I receive a notification of request
  	And two options of Accept and Reject is given
	Then I click accept
	Then I should receive an information accepted

Scenario: Reject Booking
  	Given I receive a notification of request
  	And two options of Accept and Reject is given
	Then I click Reject
	Then I should receive an information as Rejected

